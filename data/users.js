let users = [
  { id: 1, email: "abc@mail.com", token: "a-b-c" },
  { id: 2, email: "def@mail.com", token: "d-e-f" }
];

module.exports = users;